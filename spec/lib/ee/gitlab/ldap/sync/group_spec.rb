require 'spec_helper'

describe EE::Gitlab::LDAP::Sync::Group, lib: true do
  include LdapHelpers

  let(:sync_group) { described_class.new(group, proxy(adapter)) }
  let(:user) { create(:user) }

  before do
    create(:identity, user: user, extern_uid: user_dn(user.username))

    stub_ldap_config(active_directory: false)
    stub_ldap_group_find_by_cn('ldap_group1', ldap_group1, adapter)
  end

  describe '#update_permissions' do
    let(:group) do
      create(:group_with_ldap_group_link,
             cn: 'ldap_group1',
             group_access: ::Gitlab::Access::DEVELOPER)
    end

    context 'with all functionality against one LDAP group type' do
      context 'with basic add/update actions' do
        let(:ldap_group1) { ldap_group_entry(user_dn(user.username)) }

        it 'fails a stuck group older than 1 hour' do
          group.start_ldap_sync
          group.update_column(:ldap_sync_last_sync_at, 61.minutes.ago)

          expect(group).to receive(:mark_ldap_sync_as_failed)

          sync_group.update_permissions
        end

        context 'when the group ldap sync is already started' do
          it 'logs a debug message' do
            group.start_ldap_sync

            expect(Rails.logger).to receive(:debug) do |&block|
              expect(block.call).to match /^Group '\w*' is not ready for LDAP sync. Skipping/
            end.at_least(1).times

            sync_group.update_permissions
          end

          it 'does not add new members' do
            group.start_ldap_sync

            sync_group.update_permissions

            expect(group.members.pluck(:user_id)).not_to include(user.id)
          end
        end

        it 'adds new members' do
          sync_group.update_permissions

          expect(group.members.pluck(:user_id)).to include(user.id)
        end

        it 'converts an existing membership access request to a real member' do
          group.members.create(
            user: user,
            access_level: ::Gitlab::Access::MASTER,
            requested_at: DateTime.now
          )
          # Validate that the user is properly created as a requester first.
          expect(group.requesters.pluck(:user_id)).to include(user.id)

          sync_group.update_permissions

          expect(group.members.pluck(:user_id)).to include(user.id)
          expect(group.members.find_by(user_id: user.id).access_level)
            .to eq(::Gitlab::Access::DEVELOPER)
        end

        it 'downgrades existing member access' do
          # Create user with higher access
          group.add_users([user],
                          ::Gitlab::Access::MASTER, skip_notification: true)

          sync_group.update_permissions

          expect(group.members.find_by(user_id: user.id).access_level)
            .to eq(::Gitlab::Access::DEVELOPER)
        end

        it 'upgrades existing member access' do
          # Create user with lower access
          group.add_users([user],
                          ::Gitlab::Access::GUEST, skip_notification: true)

          sync_group.update_permissions

          expect(group.members.find_by(user_id: user.id).access_level)
            .to eq(::Gitlab::Access::DEVELOPER)
        end

        it 'uses the ldap sync state machine' do
          expect(group).to receive(:start_ldap_sync)
          expect(group).to receive(:finish_ldap_sync)

          sync_group.update_permissions
        end
      end

      context 'when existing user is no longer in LDAP group' do
        let(:ldap_group1) do
          ldap_group_entry(user_dn('some_user'))
        end

        it 'removes the user from the group' do
          group.add_users([user],
                          Gitlab::Access::MASTER, skip_notification: true)

          sync_group.update_permissions

          expect(group.members.find_by(user_id: user.id)).to be_nil
        end

        it 'refuses to delete the last owner' do
          group.add_users([user],
                          Gitlab::Access::OWNER, skip_notification: true)

          sync_group.update_permissions

          expect(group.members.find_by(user_id: user.id).access_level)
            .to eq(::Gitlab::Access::OWNER)
        end
      end

      context 'when the user is the last owner' do
        let(:user1) { create(:user) }
        let(:user2) { create(:user) }
        let(:ldap_group1) do
          ldap_group_entry(%W(#{user_dn(user1.username)} #{user_dn(user2.username)}))
        end

        it 'downgrades one user but not the other' do
          create(:identity, user: user1, extern_uid: user_dn(user1.username))
          create(:identity, user: user2, extern_uid: user_dn(user2.username))
          group.add_users([user1, user2],
                          Gitlab::Access::OWNER, skip_notification: true)

          sync_group.update_permissions

          expect(group.members.pluck(:access_level).sort)
            .to eq([::Gitlab::Access::DEVELOPER, ::Gitlab::Access::OWNER])
        end
      end
    end

    # Test that membership can be resolved for all different type of LDAP groups
    context 'with different LDAP group types' do
      # GroupOfNames - OpenLDAP
      context 'with groupOfNames style LDAP group' do
        let(:ldap_group1) do
          ldap_group_entry(
            user_dn(user.username),
            objectclass: 'groupOfNames',
            member_attr: 'uniqueMember'
          )
        end

        it 'adds the user to the group' do
          sync_group.update_permissions

          expect(group.members.pluck(:user_id)).to include(user.id)
        end
      end

      # posixGroup - Apple Open Directory
      context 'with posixGroup style LDAP group' do
        let(:ldap_group1) do
          ldap_group_entry(
            user.username,
            objectclass: 'posixGroup',
            member_attr: 'memberUid'
          )
        end
        let(:ldap_user) do
          ldap_user_entry(user.username)
        end

        it 'adds the user to the group' do
          stub_ldap_person_find_by_uid(user.username, ldap_user)

          sync_group.update_permissions

          expect(group.members.pluck(:user_id)).to include(user.id)
        end
      end

      context 'with groupOfUniqueNames style LDAP group' do
        let(:ldap_group1) do
          ldap_group_entry(
            user_dn(user.username),
            objectclass: 'groupOfUniqueNames',
            member_attr: 'uniqueMember'
          )
        end

        it 'adds the user to the group' do
          sync_group.update_permissions

          expect(group.members.pluck(:user_id)).to include(user.id)
        end
      end

      context 'with an empty LDAP group' do
        let(:ldap_group1) do
          ldap_group_entry(nil)
        end

        it 'does nothing, without failure' do
          expect { sync_group.update_permissions }
            .not_to change { group.members.count }
        end
      end
    end
  end
end
